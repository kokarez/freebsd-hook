#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/syscall.h>
#include <sys/sysproto.h>

static int hook_exec(struct thread *td, void *syscall_args) {
  struct execve_args *eargs = syscall_args;
  int                err = 0;

  uprintf("exec: %s ", eargs->fname);
  err = sys_execve(td, syscall_args);
  uprintf("res: %d\n", err);
  return err;
}

static int hook_exec_load(struct module *module, int cmd, void *arg) {
  int err = 0;

  switch (cmd) {
    case MOD_LOAD:
      sysent[SYS_execve].sy_call = (sy_call_t *)hook_exec;
      break;
    case MOD_UNLOAD:
      sysent[SYS_execve].sy_call = (sy_call_t *)sys_execve;
      break;
    default:
      err = EOPNOTSUPP;
  }

  return err;
}

static moduledata_t hook_exec_mod = {
  "hook_exec",
  hook_exec_load,
  NULL
};

DECLARE_MODULE(hook_exec, hook_exec_mod, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
